json.array!(@slides) do |slide|
  json.extract! slide, :desc, :author, :picture
  json.url slide_url(slide, format: :json)
end