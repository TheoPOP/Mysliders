class Slide < ApplicationRecord
   has_attached_file :picture, styles: { small: "64x64", med: "100x100", large: "200x200" }
   validates_attachment :picture,
   :content_type => { :content_type => ["image/jpeg", "image/jpg", "image/gif", "image/png"] },
   :size => { :in => 0..10000.kilobytes }
end
